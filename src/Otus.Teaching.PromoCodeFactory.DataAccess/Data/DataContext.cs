﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class DataContext : DbContext
    {
        public DbSet<Role> Roles { get; set; }

        public DbSet<Employee> Employees { get; set; }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<Preference> Preferences { get; set; }

        public DbSet<PromoCode> PromoCodes { get; set; }

        public DbSet<CustomerPreference> CustomerPreferences { get; set; }

        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Связь * - * для клинта и предпочтения
            modelBuilder.Entity<CustomerPreference>()
                .HasKey(c => new {c.CustomerId, c.PreferenceId});

            modelBuilder.Entity<CustomerPreference>()
                .HasOne(c => c.Customer)
                .WithMany(c => c.Preferences)
                .HasForeignKey(c => c.CustomerId);

            modelBuilder.Entity<CustomerPreference>()
                .HasOne(c => c.Preference)
                .WithMany(c => c.Customers)
                .HasForeignKey(c => c.PreferenceId);

            // Связь 1 - * для клиента и промокода
            modelBuilder.Entity<PromoCode>()
                .HasOne(c => c.Customer)
                .WithMany(c => c.PromoCodes)
                .OnDelete(DeleteBehavior.Cascade);


            modelBuilder.Entity<Role>().HasData(FakeDataFactory.Roles);
            modelBuilder.Entity<Employee>().HasData(FakeDataFactory.Employees);
            modelBuilder.Entity<Customer>().HasData(FakeDataFactory.Customers);
            modelBuilder.Entity<Preference>().HasData(FakeDataFactory.Preferences);
            modelBuilder.Entity<CustomerPreference>().HasData(FakeDataFactory.CustomerPreferences);


            base.OnModelCreating(modelBuilder);
        }
    }
}