﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly DbSet<T> _set;

        private readonly DataContext _db;


        public EfRepository(DataContext dataContext)
        {
            _db = dataContext ?? throw new ArgumentNullException(nameof(dataContext));
            _set = _db.Set<T>();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            return await _set.FirstOrDefaultAsync(p => p.Id == id);
        }
        
        public Task<T> GetByAsync(Func<T, bool> predicate)
        {
            return Task.FromResult(_set.FirstOrDefault(predicate));
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _set.ToListAsync();
        }

        public async Task AddAsync(T entity)
        {
            await _set.AddAsync(entity);
            await _db.SaveChangesAsync();
        }

        public async Task AddRangeAsync(IEnumerable<T> entities)
        {
            await _set.AddRangeAsync(entities);
            await _db.SaveChangesAsync();
        }

        public async Task UpdateAsync(T entity)
        {
            _set.Update(entity);
            await _db.SaveChangesAsync();
        }

        public async Task RemoveAsync(T entity)
        {
            _set.Remove(entity);
            await _db.SaveChangesAsync();
        }

        
    }
}