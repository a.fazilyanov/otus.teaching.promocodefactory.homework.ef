﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Customer, CustomerShortResponse>();
            CreateMap<Customer, CustomerResponse>().ForMember(p => p.Preferences,
                opt => opt.MapFrom(p => p.Preferences.Select(s => s.Preference).ToList()));
            CreateMap<CreateOrEditCustomerRequest, Customer>()
                .ForMember(p => p.Preferences, opt => opt.Ignore());
            CreateMap<Preference, PreferenceResponse>();
            CreateMap<GivePromoCodeRequest, PromoCode>();
            CreateMap<PromoCode,PromoCodeShortResponse>();
        }
    }
}