﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController : ControllerBase
    {
        private readonly IRepository<PromoCode> _promoCodeRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IMapper _mapper;

        public PromocodesController(
            IMapper mapper,
            IRepository<PromoCode> promoCodeRepository,
            IRepository<Preference> preferenceRepository
        )
        {
            _mapper = mapper;
            _promoCodeRepository = promoCodeRepository;
            _preferenceRepository = preferenceRepository;

        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var promoCodes = await _promoCodeRepository.GetAllAsync();
            return Ok(_mapper.Map<IEnumerable<PromoCodeShortResponse>>(promoCodes));

        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var preference = await _preferenceRepository
                .GetByAsync(p => string.Equals(p.Name, request.Preference, StringComparison.OrdinalIgnoreCase));

            if (preference == null)
            {
                return BadRequest($"Preference '{request.Preference}' not found");
            }

            var promoCodesToAdd = preference.Customers
                .Select(p => new PromoCode
                {
                    BeginDate = DateTime.MinValue,
                    EndDate = DateTime.MaxValue,
                    Customer = p.Customer,
                    Preference = preference,
                    Code = request.PromoCode,
                    PartnerName = request.PartnerName,
                    ServiceInfo = request.ServiceInfo

                })
                .ToList();
            
            await _promoCodeRepository.AddRangeAsync(promoCodesToAdd);

            return Ok();
        }
    }
}