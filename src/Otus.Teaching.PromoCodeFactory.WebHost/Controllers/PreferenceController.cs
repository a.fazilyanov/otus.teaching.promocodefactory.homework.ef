﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения 
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferenceController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IRepository<Preference> _preferenceRepository;

        public PreferenceController(
            IMapper mapper,
            IRepository<Preference> preferenceRepository)
        {
            _mapper = mapper;
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получить список всех предпочтений
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CustomerShortResponse>>> GetPreferenceAsync()
        {
            var preferenceList = await _preferenceRepository.GetAllAsync();
            return Ok(_mapper.Map<IEnumerable<PreferenceResponse>>(preferenceList));
        }
    }
}