﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;

        public CustomersController(
            IMapper mapper,
            IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository)
        {
            _mapper = mapper;
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Получить данные по всем клиентам
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CustomerShortResponse>>> GetCustomersAsync()
        {
            var customerList = await _customerRepository.GetAllAsync();
            return Ok(_mapper.Map<IEnumerable<CustomerShortResponse>>(customerList));
        }

        /// <summary>
        /// Получить данные клиентв
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            var ret = _mapper.Map<CustomerResponse>(customer);
            return Ok(ret);
        }

        /// <summary>
        /// Создать запись клиента
        /// </summary>
        /// <param name="request">Данные клиента</param>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var customer = _mapper.Map<Customer>(request);
            customer.Preferences = new List<CustomerPreference>();

            foreach (var prefId in request.PreferenceIds)
            {
                var foundedPreference = await _preferenceRepository.GetByIdAsync(prefId);

                if (foundedPreference == null)
                {
                    return BadRequest($"Preference '{prefId}' not found");
                }

                customer.Preferences.Add(
                    new CustomerPreference
                    {
                        Customer = customer,
                        Preference = foundedPreference
                    }
                );
            }

            await _customerRepository.AddAsync(customer);

            return Ok();
        }

        /// <summary>
        /// Изменить данные клиента
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
            {
                return NotFound();
            }

            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;

            customer.Preferences.Clear();
            foreach (var prefId in request.PreferenceIds)
            {
                var foundedPreference = await _preferenceRepository.GetByIdAsync(prefId);

                if (foundedPreference == null)
                {
                    return BadRequest($"Preference '{prefId}' not found");
                }
                
                customer.Preferences.Add(
                    new CustomerPreference
                    {
                        Customer = customer,
                        Preference = foundedPreference
                    }
                );
            }

            await _customerRepository.UpdateAsync(customer);

            return Ok();
        }

        /// <summary>
        /// Удалить данные клиента
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);

            if (customer == null)
            {
                return NotFound();
            }

            await _customerRepository.RemoveAsync(customer);

            return Ok();
        }
    }
}