﻿using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class GivePromoCodeRequest
    {
        public string ServiceInfo { get; set; }

        [Required]
        public string PartnerName { get; set; }

        [Required]
        public string PromoCode { get; set; }

        [Required]
        public string Preference { get; set; }
    }
}